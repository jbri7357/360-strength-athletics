$(document).ready(function() {

      $('#facility-carousel').slick({
        fade: true,
        dots: true,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 3000,
        pauseOnHover: false,
        arrows: false
      });


      $('#training-carousel').slick({
        fade: true,
        dots: true,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 3000,
        pauseOnHover: false,
        arrows: false
      });

      $(window).on("hashchange", function() {
        if (location.hash.length !== 0) {
          window.scrollTo(window.scrollX, window.scrollY - $('.navbar').height());
        }
      });



      $("#contact-form").submit(function(event) {

          /* stop form from submitting normally */
          event.preventDefault();

          /* get some values from elements on the page: */
          var $form = $(this),
            name = $('#name-input').val(),
            replyToEmail = $('#email-input').val(),
            message = $('#message-input').val(),
            subject = 'I want to join your gym';

          /* Send the data */
          $.ajax({
            url: "//formspree.io/info@360strengthathletics.com",
            method: "POST",
            data: {
              name: name,
              email: replyToEmail,
              subject: subject,
              message: message
            },
            dataType: "json"
          });

          /* Close the modal */
          $('#contact-modal').modal('hide');
        });


        transitionSpeed = 1;

        function getNavbarHeightOffset() {
          return $('.navbar').height() + 1;
        }

        function makeNavTransparent() {
          $(".navbar").animate({
            backgroundColor: 'transparent',
            borderColor: 'transparent'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-toggle .icon-bar").animate({
            backgroundColor: 'white'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-collapse").animate({
            borderColor: 'white'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-brand, .navbar-inverse .navbar-brand:hover, .navbar-inverse .navbar-brand:focus, .navbar-inverse .navbar-nav >li > a, .navbar-inverse .navbar-nav >li > a:hover, .navbar-inverse .navbar-nav >li > a:focus, .navbar-inverse .navbar-nav >li > .navbar-btn").animate({
            color: 'white'
          }, transitionSpeed);
        }

        function makeNavBlue() {
          $(".navbar").animate({
            backgroundColor: '#337ab7',
            borderColor: 'black'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-toggle .icon-bar").animate({
            backgroundColor: 'white'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-collapse").animate({
            borderColor: 'white'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-brand, .navbar-inverse .navbar-brand:hover, .navbar-inverse .navbar-brand:focus, .navbar-inverse .navbar-nav >li > a, .navbar-inverse .navbar-nav >li > a:hover, .navbar-inverse .navbar-nav >li > a:focus, .navbar-inverse .navbar-nav >li > .navbar-btn").animate({
            color: 'white'
          }, transitionSpeed);
        }

        function makeNavWhite() {
          $(".navbar").animate({
            backgroundColor: 'white',
            borderColor: 'black'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-toggle .icon-bar").animate({
            backgroundColor: 'black'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-collapse").animate({
            borderColor: 'black'
          }, transitionSpeed);

          $(".navbar-inverse .navbar-brand, .navbar-inverse .navbar-brand:hover, .navbar-inverse .navbar-brand:focus, .navbar-inverse .navbar-nav >li > a, .navbar-inverse .navbar-nav >li > a:hover, .navbar-inverse .navbar-nav >li > a:focus, .navbar-inverse .navbar-nav >li > .navbar-btn").animate({
            color: 'black'
          }, transitionSpeed);
        }

        $(".transparent-to-blue-nav").waypoint(function(direction) {
          if (direction === 'down') {
            makeNavBlue();
          } else {
            makeNavTransparent();
          }
        }, {
          offset: getNavbarHeightOffset
        });

        $(".blue-to-white-nav").waypoint(function(direction) {
          if (direction === 'down') {
            makeNavWhite();
          } else {
            makeNavBlue();
          }
        }, {
          offset: getNavbarHeightOffset
        });

        $(".white-to-blue-nav").waypoint(function(direction) {
          if (direction === 'down') {
            makeNavBlue();
          } else {
            makeNavWhite();
          }
        }, {
          offset: getNavbarHeightOffset
        });
      });
